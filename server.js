const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const router = require("./controller");
const app = new Koa();
app.use(bodyParser());

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
  }
});
app.use(router.routes());
const port = process.env.HTTP_PORT || 3000;
module.exports = app.listen(port);
console.log(`Server stared on ${port}`);
