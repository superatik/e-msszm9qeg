const unpack9 = (row, out) => {
  if (row[0] === 10) {
    out.push([row.shift()]);
    return unpack9(row, out);
  }
  if (row[0] + row[1] === 10) {
    out.push([row.shift(), row.shift()]);
    return unpack9(row, out);
  }
  if (row.length) {
    out.push(row);
  }
};

const sum = array => (array ? array.reduce((c, n) => c + n, 0) : 0);

class Score {
  //parsing
  static process(frames) {
    return Score.calculate(
      frames.reduce((p, c, i, a) => {
        const one = Object.values(c);
        if (i === 9) {
          unpack9(one, p);
        } else {
          p.push(one);
        }
        return p;
      }, [])
    );
  }
  static calculate(items) {
    return items.reduce(
      (p, c, i, a) => {
        //last3strikes
        if (i > 8 && c[0] == 10) {
          p.score += c[0];
          return p;
        }
        //strike
        if (c[0] === 10) {
          p.score += c[0] + sum(a[i + 1]) + sum(a[i + 2]);
          return p;
        }
        //spare
        if (c[0] + c[1] === 10) {
          p.score += a[i + 1][0] || 0;
        }
        //open
        p.score += c[0] + c[1] || 0;
        return p;
      },
      { score: 0 }
    ).score;
  }
}
module.exports = Score;
