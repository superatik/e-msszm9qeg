const should = require("should");
const score = require("../score");
describe("Score", function() {
  it("should count plain score", () => {
    const value = score.calculate([[2, 3], [4, 5]]);
    value.should.equal(14);
  });
  it("should count spare", () => {
    const value = score.calculate([[5, 5], [4, 1]]);
    value.should.equal(19);
  });
  it("should count strike", () => {
    const value = score.calculate([[10], [4, 1]]);
    value.should.equal(20);
  });
  it("should count spare full", () => {
    const value = score.calculate([
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9, 1],
      [9]
    ]);
    value.should.equal(190);
  });
  it("should count strike full", () => {
    const value = score.calculate([
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10]
    ]);
    value.should.equal(300);
  });
  it("should count zero plus strike", () => {
    const value = score.calculate([
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [10],
      [1, 0]
    ]);
    value.should.equal(12);
  });
});
