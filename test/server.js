const request = require("supertest");
const app = require("../server");
const should = require("should");

describe("HTTP", function() {
  it("should empty data", () => {
    return request(app)
      .post("/game")
      .expect(204);
  });

  it("should have an empty list", () => {
    return request(app)
      .get("/scores")
      .expect(200)
      .then(response => {
        response.body.should.deepEqual({ frames: [], total: 0 });
      });
  });

  it("should accept put", () => {
    return request(app)
      .put("/scores")
      .send({ first: 4, second: 5 })
      .expect(204);
  });

  it("should show list", () => {
    return request(app)
      .get("/scores", { first: 4, second: 5 })
      .expect(200)
      .then(response => {
        response.body.should.deepEqual({
          frames: [{ first: 4, second: 5 }],
          total: 9
        });
      });
  });

  it("should not accept more than 10", async () => {
    let current = await request(app).get("/scores");
    for (let i = 0; i < 9; i++) {
      await request(app)
        .put("/scores")
        .send({ first: 4, second: 5 })
        .expect(204);
    }
    current = await request(app).get("/scores");
    await request(app)
      .put("/scores")
      .send({ first: 4, second: 5 })
      .expect(406);
  });

  after(() => {
    app.close();
  });
});
