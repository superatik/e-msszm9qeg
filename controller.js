const Router = require("koa-router");
const router = new Router();
const db = require("./db");
const score = require("./score");

router.post("/game", async ctx => {
  await db.clean();
  ctx.status = 204;
});

router.put("/scores", async ctx => {
  const current = await db.list();
  if (current.frames.length > 9) {
    throw { message: "The  game is ended", status: 406 };
  }
  const { first, second, third } = ctx.request.body;
  //validation of input params
  if (
    ![first, second, third].filter(v => v && Number.isInteger(v)).length ||
    (first > 10 || second > 10 || third > 10) ||
    (current.frames.length !== 9 && first === 10 && (second || third)) ||
    (current.frames.length !== 9 && third)
  ) {
    throw { message: "Input Values are wrong", status: 400 };
  }
  //
  current.frames.push({ first, second, third });
  current.total = score.process(current.frames);
  await db.write(current);
  ctx.status = 204;
});

router.get("/scores", async ctx => {
  const out = await db.list();
  ctx.status = 200;
  ctx.body = out;
});

module.exports = router;
