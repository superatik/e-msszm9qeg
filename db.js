const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const Memory = require("lowdb/adapters/Memory");
require("dotenv").config();

class Db {
  constructor() {
    this.initial = { frames: [], total: 0 };
    this.handler = low(
      process.env.NODE_ENV === "test"
        ? new Memory()
        : new FileSync(process.env.DB_FILE)
    );
    return this;
  }
  list() {
    return this.handler.get("data").value();
  }
  write(data) {
    return this.handler.set("data", data).write();
  }
  clean() {
    this.handler.set("data", this.initial).write();
    return this.handler.get("data").value();
  }
}
module.exports = new Db();
